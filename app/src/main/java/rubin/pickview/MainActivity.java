package rubin.pickview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bruce.pickerview.popwindow.DatePickerPopWin;

public class MainActivity extends AppCompatActivity {

    Button btnDatePick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnDatePick = (Button) findViewById(R.id.btnDatePick);
    }

    public void showDatePick(View view) {
        DatePickerPopWin pickerPopWin = new DatePickerPopWin(MainActivity.this, new DatePickerPopWin.OnDatePickedListener() {
            @Override
            public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                //handler the result here
                Toast.makeText(MainActivity.this, dateDesc, Toast.LENGTH_SHORT).show();
            }
        });
        pickerPopWin.showPopWin(MainActivity.this);
    }
}
